alias r=radian

source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"

zinit wait lucid for \
 atinit"ZINIT[COMPINIT_OPTS]=-C; zicompinit; zicdreplay" \
    zdharma-continuum/fast-syntax-highlighting \
 blockf \
    zsh-users/zsh-completions \
 atload"!_zsh_autosuggest_start" \
    zsh-users/zsh-autosuggestions
zinit light romkatv/powerlevel10k
zinit light agkozak/zsh-z
zinit light z-shell/zsh-eza
zinit snippet OMZL::completion.zsh
zinit snippet OMZL::directories.zsh
zinit snippet OMZL::key-bindings.zsh
zinit snippet OMZL::history.zsh
zinit snippet OMZP::archlinux
zinit snippet OMZP::colored-man-pages
zinit snippet OMZP::cp
zinit snippet OMZP::git
zinit snippet OMZP::systemd

eval "$(atuin init zsh --disable-up-arrow)"
source /etc/grc.zsh
source ~/.p10k.zsh