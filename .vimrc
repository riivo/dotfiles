colorscheme nord

set list
set nu
set hlsearch
set tabstop=2
set shiftwidth=2
set expandtab
set nofixendofline
let g:airline_powerline_fonts=1

" Plugins in .vim/pack/plugins/start

" git clone https://github.com/arcticicestudio/nord-vim
" git clone https://github.com/jeffkreeftmeijer/vim-numbertoggle
" git clone https://github.com/mechatroner/rainbow_csv
" git clone https://github.com/tpope/vim-sensible
" git clone https://github.com/vim-airline/vim-airline
" git clone https://github.com/Yggdroot/indentLine

" Update them by running (in .vim/pack/plugins/start)
" for plugin in *; do git -C $plugin pull; done